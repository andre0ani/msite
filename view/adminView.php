<!-- Page principal de l'administration -->

<?php session_start();

if (isset($_COOKIE['admin']) && !empty($_COOKIE['admin']) && 
    (isset($_SESSION['admin']) && !empty($_SESSION['admin']))) 
{
    $admin = session_id().microtime().rand(0,9999999999);
    $admin = hash('sha512', $ticket);
    $_COOKIE['admin'] = $admin;
    $_SESSION['admin'] = $admin;
}
else
{
    $_SESSION = array();
    session_destroy();
    header('Location: index.php?action=listPosts');
}
?>


<p>Vous pouvez gérer le site à l'aide des menus à gauche.</p>

<br><br>

<p><h3>Informations sur le serveur</h3></p>
<ul>
<li>Système d'exploitation : <?php echo php_uname('s'); ?></li>
<li>Nom d'hôte : <?php echo php_uname('n'); ?></li>
<li>Architecture : <?php echo php_uname('m'); ?></li>
<li>Version de PHP : <?php echo phpversion(); ?></li>
<li>Mail de l'administrateur : <?php echo $_SERVER['SERVER_ADMIN'] ?></li>
<li>I.P. du serveur : <?php echo $_SERVER['SERVER_ADDR'] ?></li>
<li>Domaine : <?php echo $_SERVER['HTTP_HOST'] ?></li>
</ul>

<br><br>

<p><h3>Informations sur le site</h3></p>
        <form action="index.php?action=writeInformations" method="post">
        <label for="titre">Titre du site :</label>
        <input id="titre" name="titre" type="text" />
        <br>
        <label for="redacteur">Rédacteur :</label>
        <input id="redacteur" name="redacteur" type="text" />
        <br>
        <label for="description">Description :</label>
        <input id="description" name="description" type="text" />
        <br>
        <label for="keywords">Mots-clé :</label>
        <input id="keywords" name="keywords" type="text" />
        <br>
        <label for="theme">Thème :</label>
        <select name="theme" id="theme">
    <option value="">Choisir un thème</option>
    <option value="css/style.css">Classic</option>
    <option value="css/blue.css">Bleu</option>
</select>
        <br>
            <input type="submit" name="valider" value="Envoyer"/>
        </form>

<?php $contenu = ob_get_clean(); ?>

<?php require 'templates/templateBack.php'; ?>