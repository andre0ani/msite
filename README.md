## MSite
## Outil de création de simple simple

MSite permet de créer simplement un petit site internet, de façon rapide et légère.
Il n'utilise pas de base de données, mais de simples fichiers textes, et est développé en pHP avec Bootstrap.
