<?php

namespace controller;

use Exception;

// Controler post

class PostController extends Controller
{

    // Affichage des articles
    function listPosts($title)
    {
        require('view/indexView.php');
    }

    // Rédaction du footer
    function writePostF($contenu)
    {

        $footerF = fopen('pages/footer.txt', 'w');
        fwrite($footerF, $contenu);
        fclose($footerF);

        header('Location: index.php?action=administration');
    }



    // Rédaction de accueil
    function writePAccueil($contenu)
    {

        $accueilF = fopen('pages/accueil.txt', 'w');
        fwrite($accueilF, $contenu);
        fclose($accueilF);

        header('Location: index.php?action=administration');
    }


    // Rédaction de a propos
    function writePropos($contenu)
    {

        $proposF = fopen('pages/propos.txt', 'w');
        fwrite($proposF, $contenu);
        fclose($proposF);

        header('Location: index.php?action=administration');
    }



    // Rédaction de contact
    function writeContact($contenu)
    {
        $contactF = fopen('pages/contact.txt', 'w');
        fwrite($contactF, $contenu);
        fclose($contactF);

        header('Location: index.php?action=administration');
    }


    // Rédaction de presentation
    function writePresentation($contenu)
    {

        $presentationF = fopen('pages/presentation.txt', 'w');
        fwrite($presentationF, $contenu);
        fclose($presentationF);

        header('Location: index.php?action=administration');
    }


    // Rédaction de mentions
    function writeMentions($contenu)
    {

        $legalsF = fopen('pages/legals.txt', 'w');
        fwrite($legalsF, $contenu);
        fclose($legalsF);
        header('Location: index.php?action=administration');
    }


    // Rédaction des informations du site
    function writeInformations($titre, $redacteur, $description, $keywords, $theme)
    {

        $data = $titre . PHP_EOL . $redacteur . PHP_EOL . $description . PHP_EOL . $keywords . PHP_EOL . $theme . PHP_EOL;
        $fp = fopen('informations/informations.txt', 'w');
        fwrite($fp, $data);

        header('Location: index.php?action=administration');
    }


    // envoie vers la page de rédaction du footer
    function viewWriteF()
    {
        require('view/writeFView.php');
    }


    // envoie vers la page de rédaction de accueil
    function viewWriteAccueil()
    {
        require('view/writeAccueilView.php');
    }


    // envoie vers la page de rédaction de a propos
    function viewWritePropos()
    {
        require('view/writeProposView.php');
    }


    // envoie vers la page de rédaction de contact
    function viewWriteContact()
    {
        require('view/writeContactView.php');
    }



    // envoie vers la page de rédaction de presentation
    function viewWritePresentation()
    {
        require('view/writePresentationView.php');
    }



    // envoie vers la page de rédaction de mentions
    function viewWriteLegales()
    {
        require('view/writeLegalesView.php');
    }
}
