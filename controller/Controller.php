<?php

namespace controller;

// Controler principal

class Controller
{

    // page � propos
    public function aboutAuthor($title)
    {
        require 'view/aboutView.php';
    }

    // page mentions legales
    public function legalView($title)
    {
        require 'view/legalView.php';
    }

    // page presentation
    public function presentationView($title)
    {
        require 'view/presentationView.php';
    }

    // page contact
    public function contactView($title)
    {
        require 'view/contactView.php';
    }

    // page d'erreur
    public function error404()
    {
        require '../view/errorView.php';
    }

    // envoie vers la page d'administration
    public function administration()
    {
        require 'view/adminView.php';
    }

        // envoie vers la page visiteurs
        public function visiteursView()
        {
            require 'view/viewVisiteurs.php';
        }
}
