<?php

// Routeur de l'application

// Redirige toutes les requêtes utilisateur vers les
// bonnes pages et actions

//use controller\ComController;
use controller\Controller;
use controller\PostController;
use controller\UserController;

// Appel des différents controlers
require_once './controller/Controller.php';
require_once './controller/UserController.php';
require_once './controller/PostController.php';


// Routes des actions et requêtes

try {
    // page d'accueil avec tous les articles
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'listPosts') {
            $infos = new PostController();
            $title = 'Accueil';
            $infos->listPosts($title);
        }

        // écrire le footer
        elseif ($_GET['action'] == 'writePostFoot') {
            if (!empty($_POST['contenu'])) {
                $infos = new PostController();
                $infos->writePostF($_POST['contenu']);
            } else {
                throw new Exception('Tous les champs ne sont pas remplis');
            }
        }

        // écrire la page accueil
        elseif ($_GET['action'] == 'writeAccueil') {
            if (!empty($_POST['contenu'])) {
                $infos = new PostController();
                $infos->writePAccueil($_POST['contenu']);
            } else {
                throw new Exception('Tous les champs ne sont pas remplis');
            }
        }

        // écrire la page a propos
        elseif ($_GET['action'] == 'writePropos') {
            if (!empty($_POST['contenu'])) {
                $infos = new PostController();
                $infos->writePropos($_POST['contenu']);
            } else {
                throw new Exception('Tous les champs ne sont pas remplis');
            }
        }

        // écrire la page contact
        elseif ($_GET['action'] == 'writeContact') {
            if (!empty($_POST['contenu'])) {
                $infos = new PostController();
                $infos->writeContact($_POST['contenu']);
            } else {
                throw new Exception('Tous les champs ne sont pas remplis');
            }
        }

        // écrire la page mentions
        elseif ($_GET['action'] == 'writeMentions') {
            if (!empty($_POST['contenu'])) {
                $infos = new PostController();
                $infos->writeMentions($_POST['contenu']);
            } else {
                throw new Exception('Tous les champs ne sont pas remplis');
            }
        }

        // écrire la page presentation
        elseif ($_GET['action'] == 'writePresentation') {
            if (!empty($_POST['contenu'])) {
                $infos = new PostController();
                $infos->writePresentation($_POST['contenu']);
            } else {
                throw new Exception('Tous les champs ne sont pas remplis');
            }
        }

        // écrire les informations du site
        elseif ($_GET['action'] == 'writeInformations') {
            if (!empty($_POST['titre']) || !empty($_POST['redacteur']) || !empty($_POST['description']) || !empty($_POST['keywords']) || !empty($_POST['theme'])) {
                $infos = new PostController();
                $infos->writeInformations($_POST['titre'], $_POST['redacteur'], $_POST['description'], $_POST['keywords'], $_POST['theme']);
            } else {
                throw new Exception('Il faut remplir au moins un champ');
            }
        }

        // connection à l'admin
        elseif ($_GET['action'] == 'logAdminF') {
            if ((isset($_POST['user']) && !empty($_POST['user'])) && (isset($_POST['pass']) && !empty($_POST['pass']))) {
                $infos = new UserController();
                $infos->logAdmin();
            } else {
                throw new Exception('Tous les champs ne sont pas remplis');
            }
        }

        // envoie vers la page d'administration
        elseif ($_GET['action'] == 'administration') {
            if (isset($_COOKIE["admin"])) {
                $infos = new Controller();
                $infos->administration();
            } else {
                header('Location: index.php?action=connection');
            }
        }

        // envoie vers la page d'erreur
        elseif ($_GET['action'] == 'error') {
            $infos = new Controller();
            header('Location: index.php?action=error404');
        }

        // envoie vers la page de connection
        elseif ($_GET['action'] == 'connection') {
            $infos = new UserController();
            $infos->connectionAdmin();
        }

        // envoie vers la page d'accueil
        elseif ($_GET['action'] == '') {
            $infos = new UserController();
            $infos->listPosts();
        }

        // envoie vers la page d'accueil
        elseif ($_GET['action'] == '/') {
            $infos = new UserController();
            $infos->listPosts();
        }

        // deconnection
        elseif ($_GET['action'] == 'logout') {
            $infos = new UserController();
            $infos->connectionAdmin();
            session_start();
            session_unset();
            session_destroy();
            setcookie('admin', '', time());
            header('Location: index.php');
        }

        // envoie vers la page de rédaction du footer
        elseif ($_GET['action'] == 'viewWriteF') {
            $infos = new PostController();
            $infos->viewWriteF();
        }

        // envoie vers la page de rédaction de accueil
        elseif ($_GET['action'] == 'viewWriteAccueil') {
            $infos = new PostController();
            $infos->viewWriteAccueil();
        }

        // envoie vers la page de rédaction de a propos
        elseif ($_GET['action'] == 'viewWritePropos') {
            $infos = new PostController();
            $infos->viewWritePropos();
        }

        // envoie vers la page de rédaction de contact
        elseif ($_GET['action'] == 'viewWriteContact') {
            $infos = new PostController();
            $infos->viewWriteContact();
        }

        // envoie vers la page de rédaction de presentation
        elseif ($_GET['action'] == 'viewWritePresentation') {
            $infos = new PostController();
            $infos->viewWritePresentation();
        }

        // envoie vers la page de rédaction de mentions legales
        elseif ($_GET['action'] == 'viewWriteLegales') {
            $infos = new PostController();
            $infos->viewWriteLegales();
        }

        // envoie vers la page de gestion des articles
        elseif ($_GET['action'] == 'viewAllPost') {
            $infos = new PostController();
            $infos->allPostBack();
        }

        // envoie vers la page à propos
        elseif ($_GET['action'] == 'about') {
            $infos = new Controller();
            $title = 'A propos';
            $infos->aboutAuthor($title);
        }

        // envoie vers la page mentions légales
        elseif ($_GET['action'] == 'legal') {
            $infos = new Controller();
            $title = 'Mentions légales';
            $infos->legalView($title);
        }

        // envoie vers la page presentation
        elseif ($_GET['action'] == 'presentation') {
            $infos = new Controller();
            $title = 'Présentation';
            $infos->presentationView($title);
        }

        // envoie vers la page contact
        elseif ($_GET['action'] == 'contact') {
            $infos = new Controller();
            $title = 'Contact';
            $infos->contactView($title);
        }

        // envoie vers la page visiteurs
        elseif ($_GET['action'] == 'viewVisiteurs') {
            $infos = new Controller();
            $infos->visiteursView();
        }

        // page d'accueil
    } else {
        $infos = new PostController();
        $title = 'Accueil';
        $infos->listPosts($title);
    }
} catch (Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
}
