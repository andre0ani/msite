<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[2];  ?>" />
  <meta name="author" content="<?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[1];  ?>" />
  <meta name="keywords" content="<?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[3];  ?>">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="icon" type="image/png" href="images/favicon.ico" />

  <title><?php
          $lines = file('informations/informations.txt');
          echo $desc=$lines[0];
          echo $titre . ' - Administration';
          ?></title>



</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php?action=listPosts"><?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[0];  ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php?action=listPosts">Accueil
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?action=about">A propos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?action=presentation">Présentation</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?action=contact">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?action=legal">Mentions Légales</a>
          </li>

        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">



      <!-- Sidebar Widgets Column -->
      <div class="col-md-4 widget">

        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Site</h5>
          <div class="card-body">

            <p>Retour sur le <a href="index.php?action=listPosts" title="Retour sur l'accueil du blog">site</a></p>
            <p><a href="index.php?action=administration">Accueil Administration</a></p>
          </div>
        </div>


        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Utilisateurs</h5>
          <div class="card-body">

            <p><?php
                if (isset($_SESSION['user'])) {
                  echo 'Connecté : ' . $_SESSION['user'];
                } ?></p>
            </p>
            <p><a href="index.php?action=logout">Se déconnecter</a></p>
          </div>
        </div>



        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Site</h5>
          <div class="card-body">
            <p><a href="../index.php?action=viewWriteF" title="Publier le bas de page">Publier le bas de page</a></p>
          </div>
        </div>



        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Pages</h5>
          <div class="card-body">
            <p><a href="../index.php?action=viewWriteAccueil" title="Publier la page d'accueil">Publier la page d'accueil</a></p>
            <p><a href="../index.php?action=viewWritePropos" title="Publier la page à propos">Publier la page à propos</a></p>
            <p><a href="../index.php?action=viewWritePresentation" title="Publier la page presentation">Publier la page presentation</a></p>
            <p><a href="../index.php?action=viewWriteContact" title="Publier la page contact">Publier la page contact</a></p>
            <p><a href="../index.php?action=viewWriteLegales" title="Publier la page mentions legales">Publier la page mentions legales</a></p>
          </div>
        </div>

        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Informations</h5>
          <div class="card-body">
            <p><a href="../index.php?action=viewVisiteurs" title="Visiteurs du site">Visiteurs</a></p>
          </div>
        </div>

      </div>

      <!-- Contenu -->
      <div class="col-md-8 principal">

        <h1 class="my-4">Administration du site

        </h1>

        <div class="card my-6">
          <h5 class="card-header">Gestion du site</h5>
          <?= $contenu ?>
        </div>
      </div>

    </div>

  </div>
  <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="pied">
    MSite, création de site simple et léger    
  </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap JavaScript -->
  <script src="bootstrap/jquery/jquery-3.5.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
