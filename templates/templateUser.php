<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[2];  ?>" />
  <meta name="author" content="<?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[1];  ?>" />
  <meta name="keywords" content="<?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[3];  ?>">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="icon" type="image/png" href="images/favicon.ico" />

    <title><?php
          $lines = file('informations/informations.txt');
          echo $desc=$lines[0];
          ?></title>




  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php?action=listPosts"><?php
                  $lines = file('informations/informations.txt');
                  echo $desc=$lines[0];  ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Contenu -->
        <div class="col-md-8 principal">

          <h1 class="my-4">Connexion à l'administration</h1>
          
          <?=$contenu?>


        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4 widget">
          
          <!-- Widget -->
          <div class="card my-4">
            <h5 class="card-header">Suivez-moi</h5>
            <div class="card-body">
       <p><a href="https://mamot.fr/auth/sign_in" target="_blank"><img src="images/mastodon.png" width="32" height="34" alt="Mastodon"></a>
       <a href="https://framasphere.org/users/sign_in" target="_blank"><img src="images/diaspora.png" width="32" height="32" alt="Diaspora"></a></p>
            </div>
          </div>

          
          <!-- Widget -->
          <div class="card my-4">
            <h5 class="card-header">Administration</h5>
            <div class="card-body">

        <?php
          if (isset($_SESSION['user'])) {
            echo '<p>Connecté : ' . '<a href="index.php?action=administration">' . $_SESSION['user'] . '</a></p>';
            echo '<p><a href="index.php?action=logout">Se déconnecter</a></p>';
            } else {
            echo '<p><a href="index.php?action=connection">Se connecter</a></p>';
          }
        ?>
      </div>
    </div>
          
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="pied">
        MSite, création de site simple et léger
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap JavaScript -->
    <script src="bootstrap/jquery/jquery-3.5.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
